# LiDSiM

Please find the LiDSiM repository at [LiDSiM](https://gitlab.com/mkuhring/LiDSiM).

LiDSiM is a tool to estimate the possible influence of error-tolerant database searches and proteogenomic approaches on the amount of unidentified spectra and the ratios of taxonomic relationship of identified spectra in MS/MS studies of microbial proteomes.

For more details about LiDSiM and its functioning, please see:  
[Estimating the Computational Limits of Detection of Microbial Non-Model Organisms](http://onlinelibrary.wiley.com/doi/10.1002/pmic.201400598/abstract)  
Mathias Kuhring and Bernhard Y. Renard, 2015, Proteomics
